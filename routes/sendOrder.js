const express = require('express');
const router = express.Router();
const request = require('request');
const { telegram_chat_id: chat_id, telegram_bot_token }  = require('../config.json');
const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');

router.get('/', function(req, res, next) {
    const params = req.query;
    if (!params.orderId) {
        res.send({
            status: 'error',
            data: 'Не указан ID заказа'
        });
        return;
    }

    const ordersAdapter = new FileSync('db/db.json');
    const ordersDb = low(ordersAdapter);
    const order = ordersDb.get('orders')
    .find({ id: Number(params.orderId) })
    .value();

    if (!order) {
        res.send({
            status: 'error',
            data: 'Заказ не найден',
        });
        return;
    }

    const message = `Отправка открытки с текстом: "${ order.text }", по адресу: "${ order.address }"`;


    request(
        {
            method:'post',
            url: `https://api.telegram.org/${ telegram_bot_token }/sendMessage`,
            form: {
                chat_id: chat_id,
                text: message,
            },
            headers: {
                "content-type": "application/json",
            },
            json: true,
        },
        (err, newRes, body) => {
            if (err) {
                res.send({
                    status: 'error',
                    data: err,
                });
            } else {
                res.send({
                    status: 'ok',
                    data: body
                });
            }
        }
    );
});

module.exports = router;

const express = require('express');
const router = express.Router();
const _ = require('lodash');
const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');

router.get('/', function(req, res, next) {
    // Add a post
    const params = req.query;

    if (!params.cardId) {
        res.send({
            status: 'error',
            data: 'Не указан ID карточки'
        });
        return;
    }

    if (!params.text) {
        res.send({
            status: 'error',
            data: 'Не указан текст'
        });
        return;
    }

    if (!params.address) {
        res.send({
            status: 'error',
            data: 'Не указан адрес'
        });
        return;
    }

    const ordersAdapter = new FileSync('db/db.json');
    const postcardsAdapter = new FileSync('db/postcards.json');
    const ordersDb = low(ordersAdapter);
    const postcardsDb = low(postcardsAdapter);

    ordersDb.defaults({ orders: [] }).write();
    postcardsDb.defaults({ postcards: [] }).write();

    const card = postcardsDb.get('postcards')
        .find({ id: Number(params.cardId) })
        .value();

    if (!card) {
        res.send({
            status: 'error',
            data: 'Не удалось найти карточку',
        });
        return;
    }

    const order = {
        id: _.random(10000, 99000),
        card: card,
        text: params.text,
        address: params.address,
    };

    ordersDb.get('orders')
        .push(order)
        .write();

    res.send({
        status: 'ok',
        data: order
    });
});

module.exports = router;

const express = require('express');
const router = express.Router();

const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');
const adapter = new FileSync('db/postcards.json');
const db = low(adapter);

db.defaults({ postcards: [] }).write();

router.get('/', function(req, res, next) {
    res.send({
        status: 'ok',
        data: db.get('postcards').value()
    });
});

module.exports = router;
